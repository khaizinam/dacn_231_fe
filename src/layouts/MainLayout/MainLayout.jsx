import { Navigate, Route, Routes } from "react-router-dom";
import "./MainLayout.scss";
import Dashboard from "../../shared/components/Dashboard/Dashboard";
import Sidebar from "../../shared/components/Sidebar/Sidebar";
import Manager from "../../pages/manager/manager";
import { SCREEN } from "../../shared/constants/router.constant";
// import { SCREEN } from "../../shared/constants/router.constant";

const MainLayout = () => {
    // const user = useSelector(state => state.AuthReducer.user);
    // if (!user) return <Navigate to="/auth" />
    // else
    return (
        <div className="MainLayout">
            <div className="Sidebar-Container">
                <Sidebar />
            </div>

            <div className="Content-Container">
                <Routes>
                    <Route path="/" element={<Dashboard />} />
                    <Route path="/info" element={<div>Infomation</div>} />
                    <Route path="/manage/*" element={<Manager />}/>
                </Routes>
            </div>
        </div>
    );
}

export default MainLayout;