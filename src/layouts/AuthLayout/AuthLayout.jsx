
import { Navigate, Route, Routes } from "react-router-dom";
import Login from "../../pages/Login/Login";
import Register from "../../pages/Register/Register";

import "./AuthLayout.scss";
import { useSelector } from "react-redux";

const AuthLayout = () => {
    // const token = useSelector(state => state.AuthReducer.token);
    // if (token) return <Navigate to="/" />
    return (
        <div className="AuthLayout">
            <div className="AuthBox">
                <Routes>
                    <Route index element={<Login />} />
                    <Route path="register" element={<Register />} />
                </Routes>
            </div>
        </div>
    );
}

export default AuthLayout;