import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { SCREEN } from "./shared/constants/router.constant";
import PageNotFound from "./pages/404/pageNotFound";
import MainLayout from "./layouts/MainLayout/MainLayout";
import AuthLayout from "./layouts/AuthLayout/AuthLayout";
function App() {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route path="/auth/*" element={<AuthLayout />} />
          <Route path="/*" element={<MainLayout />} />
          <Route
            exact
            path={SCREEN._404.path}
            element={<PageNotFound />}
          ></Route>
          <Route
            exact
            path={SCREEN._404_1.path}
            element={<PageNotFound />}
          ></Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
