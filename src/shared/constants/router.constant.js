export const SCREEN = {
  homepage: {
    id: "homepage",
    path: "/",
    title: "Home Page",
  },
  s02: {
    id: "s02",
    path: "/s02",
    title: "Test Permission",
  },
  dev: {
    id: "dev",
    path: "/dev",
    title: "dev Page",
  },
  loginPage: {
    id: "loginPage",
    path: "/login",
    title: "Đăng nhập",
  },
  registerPage: {
    id: "registerPage",
    path: "/register",
    title: "Đăng ký",
  },
  mainPage: {
    id: "mainPage",
    path: "/main-page",
    title: "Trang Chủ",
  },
  manager: {
    id: "manager",
    path: "/manage",
    title: "Quản lí cây trồng",
  },
  _404: {
    id: "_404",
    path: "/404",
    title: "404 Page",
  },
  _404_1: {
    id: "s404",
    path: "/*",
    title: "404 Page",
  },
};
