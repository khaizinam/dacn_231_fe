export const CONST_MESSAGE = {
  REQUIRED: {
    code: "REQUIRED",
    vn: "{1} không được bỏ trống !",
    en: "{1} is required!",
  },
  IN_VALID: {
    code: "IN_VALID",
    vn: "{1} không đúng định dạng !",
    en: "{1} is required!",
  },
  C001: {
    code: "C001",
    vn: "Độ dài {1} phải từ {2} kí tự trở lên !",
    en: "Length of {1} is upto {2}!",
  },
};
export const CONST_ITEM = {
  ACTION_OK: "ok",
  ACTION_CLOSE: "close",
  MODAL: {
    WARNING: "warning",
    CONFIRM: "confirm",
    INFO: "info",
  },
};
export const CONST_VALIDATE = {
  EMAIL: /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$/,
};
