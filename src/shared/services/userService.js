const { default: BaseService } = require("../common/common-service");
const { URL_CONST } = require("../environment");

class UserService extends BaseService {
  onLogin(payload) {
    return this.post(URL_CONST.SERVER_URL + URL_CONST.AUTH.LOGIN, payload);
  }
}
export default UserService;
