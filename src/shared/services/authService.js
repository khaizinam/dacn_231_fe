const { default: BaseService } = require("../common/common-service");
const { URL_CONST } = require("../environment");

export default class AuthService extends BaseService {
  initPermission() {
    return this.get(URL_CONST.SERVER_URL + URL_CONST.AUTH.PERMISSION, {});
  }

  login(email, password) {
    return this.post(URL_CONST.SERVER_URL + URL_CONST.AUTH.LOGIN, {
      email: email,
      password: password,
    });
  }
  register(email, password, full_name) {
    return this.post(URL_CONST.SERVER_URL + URL_CONST.AUTH.SIGNUP, {
      email,
      password,
      full_name,
    });
  }
  logout() {
    return this.get(URL_CONST.SERVER_URL + URL_CONST.AUTH.LOGOUT, {});
    //this.clearCookie();
  }
}
