const URL_CONST = {
  SERVER_URL: process.env.REACT_APP_SERVER_URL,
  AUTH: {
    LOGIN: "/auth/login",
    SIGNUP: "/auth/sign-up",
    LOGOUT: "/auth/log-out",
    PERMISSION: "/auth/permission",
  },
};
module.exports = { URL_CONST };
