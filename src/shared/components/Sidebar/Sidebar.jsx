import { Link, NavLink } from "react-router-dom";
import Logo from "../Logo/Logo";
import { useDispatch } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faIcons, faArrowAltCircleLeft, faCannabis } from "@fortawesome/free-solid-svg-icons";
import { faEnvira } from "@fortawesome/free-brands-svg-icons";
import "./Sidebar.scss";
import { SCREEN } from "../../constants/router.constant";

const navlist = [
    {
        link: '/',
        title: "Mạng xã hội",
        icon: faIcons
    },
    {
        link: '/info',
        title: "Thư viện cây",
        icon: faEnvira
    },
    {
        link: '/manage',
        title: "Vườn của tôi",
        icon: faCannabis
    }
];

const Sidebar = () => {
    const dispatch = useDispatch();
    const handleLogOut = async () => {
    }
    return (
        <div className="Sidebar">
            <Link to="/" style={{ textDecoration: "none", color: "inherit" }}>
                <Logo />
            </Link>

            <div className="nav-list">
                {
                    navlist.map((nav, index) => (
                        <NavLink
                            key={index}
                            to={nav.link}
                            className='nav-item'
                        >
                            {({ isActive }) => (
                                <div className={`nav-container ${isActive ? "active" : ""}`}>
                                    <FontAwesomeIcon icon={nav.icon} className="nav-icon" />
                                    <span className="nav-text">{nav.title}</span>
                                </div>
                            )}

                        </NavLink>
                    ))
                }
            </div>

            <div className="more" onClick={handleLogOut}>
                <div className='nav-item'>
                    <div className="nav-container">
                        <FontAwesomeIcon icon={faArrowAltCircleLeft} className="nav-icon" />
                        <span className="nav-text">Đăng xuất</span>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Sidebar;