
import Comment from "../Comment/Comment";
import Image from "../Image/Image";


import "./PostDetail.scss";
import Textarea from "../Textarea/Textarea";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
// import { GetAllCommentsAction } from "../../redux/CommentRedux";
import { showTime } from "../../common/formatTime";

const PostDetail = ({
    id, src, fullname = "", avatar, description, createdAt,
    commentText, setCommentText, commentSubmit
}) => {

    const auth = {}
    const comments = []
    const dispatch = useDispatch();

    const [time, setTime] = useState(showTime(createdAt));

    useEffect(() => {
        const timeReset = 60000 - (new Date().getTime() - new Date(createdAt).getTime()) % 60000;
        setTimeout(() => setTime(showTime(createdAt)), timeReset);
    }, [time])

    useEffect(() => {
        // dispatch(GetAllCommentsAction(id, auth.token));
    }, [])

    return (
        <div className="PostDetail">
            <div className="left-image">
                <Image src={src ? src : "https://img.freepik.com/premium-photo/tree-small-island-with-sun-it_753098-14808.jpg"} />
            </div>

            <div className="right-comment">
                <div className="header-comment">
                    <div className="post-info">
                        <div className="avatar">
                            <img src={avatar} alt="" />
                        </div>
                        <div style={{
                            display: "flex",
                            alignItems: 'end',
                            justifyContent: 'space-between'
                        }}>
                            <div className="name">{fullname}</div>
                            <div className="dot"></div>
                            <div className="time">{time}</div>
                        </div>
                    </div>
                    <div className="more">
                        <i className='bx bx-dots-horizontal-rounded'></i>
                    </div>
                </div>

                <div className="description">
                    {description}
                </div>

                <div className="body-comment">
                    <div className="comment-number">
                        {comments.length} bình luận
                    </div>
                    {
                        comments.map((comment) => <Comment key={comment._id + new Date().getTime()} {...comment} self={auth.user._id == comment.userId} />)
                    }
                </div>

                <div className="comment-input">
                    <i className='bx bxs-pencil comment-icon'></i>
                    <Textarea
                        rows={1}
                        placeholder={"Thêm bình luận"}
                        onChange={setCommentText}
                        value={commentText}
                    />
                    <button className="comment-button" onClick={commentSubmit}>Đăng</button>
                </div>
            </div>
        </div>
    );
}

export default PostDetail;