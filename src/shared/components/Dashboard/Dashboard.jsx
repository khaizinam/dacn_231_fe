import "./Dashboard.scss";

import Feed from "../Feed/Feed";

const Dashboard = () => {
    return (
        <div className="Dashboard">
            <div className="Feed-Container">
                <Feed />
            </div>
            <div className="RightSide-Container">
                Right
            </div>
        </div>
    );
}

export default Dashboard;