import "./Comment.scss";
import { useEffect, useState } from "react";
import { showTime } from "../../common/formatTime";

const Comment = ({ self, description, fullname, avatar, createdAt }) => {
    const avatarImg = process.env.REACT_APP_BE_URL + "/file/avatars/" + (avatar || "defaultAvatar.jpg");
    const [time, setTime] = useState(showTime(createdAt));

    useEffect(() => {
        const timeReset = 60000 - (new Date().getTime() - new Date(createdAt).getTime()) % 60000;
        setTimeout(() => setTime(showTime(createdAt)), timeReset);
    }, [time])

    return (
        <div className="Comment">
            <div className={`comment-item ${self ? "self" : "other"}`}>
                <div className="comment-info">
                    <div className="image">
                        <img src={avatarImg} alt="" />
                    </div>
                    <div className="comment-content" >
                        <span className="name">{fullname} {self ? "(Bạn)" : ""}</span>
                        <span className="time">{time}  </span>
                        <p style={{ whiteSpace: "pre-wrap" }}>
                            {description}
                        </p>

                    </div>
                </div>
            </div>
        </div>
    );
}

export default Comment;