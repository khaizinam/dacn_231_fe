
import { useEffect, useState } from "react";
import Post from "../Post/Post";

import Modal from "../Modal/Modal";
import NewPost from "../NewPost/NewPost";

import "./Feed.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPenToSquare } from "@fortawesome/free-solid-svg-icons";

const Feed = () => {
    // const auth = useSelector(state => state.AuthReducer);
    const posts = [
        {
            id: 11,
            img: "https://img.freepik.com/premium-photo/tree-small-island-with-sun-it_753098-14808.jpg",
            description: "hello",
            createdAt: new Date(2023, 10, 7),
            owner: {
                id: 123,
                fullname: "Hong Khanh",
                avatar: "https://img.freepik.com/premium-photo/tree-small-island-with-sun-it_753098-14808.jpg"
            },
            liked: true,
            public: false,
        }
    ]

    // const dispatch = useDispatch();

    const [modalNewPost, setModalNewPost] = useState(false);

    useEffect(() => {
        // const fetchPosts = async () => {

        // }
        // fetchPosts();
    }, [])

    return (
        <div className="Feed">
            <h1>Your feed</h1>
            <div className="Post-list">
                {posts.map((post) => {
                    const key = (post.id + "" + new Date().getTime());
                    return <Post key={key} {...post} />
                })}
            </div>

            <div className="new-post-btn" title="Bài viết mới" onClick={() => setModalNewPost(true)}>
                <FontAwesomeIcon icon={faPenToSquare} />
            </div>

            {
                modalNewPost && <Modal onClose={() => setModalNewPost(false)}>
                    <NewPost onClose={() => setModalNewPost(false)} />
                </Modal>
            }


        </div>
    );
}

export default Feed;