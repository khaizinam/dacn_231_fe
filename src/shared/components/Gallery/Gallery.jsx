import "./Gallery.scss";
import { useEffect, useState } from "react";

const Gallery = ({ images = [] }) => {

    return (
        <div className="Gallery">
            {images.map((image, index) => <img src={image} key={index} />)}
        </div>
    );
}

export default Gallery;