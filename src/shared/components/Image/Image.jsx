import "./Image.scss";

const Image = ({ src, type = "contain" }) => {
    return (
        <div className="Image">
            <img src={src} alt="" />
        </div>
    );
}

export default Image;