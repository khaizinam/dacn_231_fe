import "./Logo.scss";

const Logo = () => {
    return (
        <div className="Logo">
            <span className="logo-text">
                Plants
            </span>
        </div>
    );
}

export default Logo;