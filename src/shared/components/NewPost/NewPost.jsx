import { useRef, useState } from "react";
import Image from "../Image/Image";
import SwitchButton from "../SwitchButton/SwitchButton";

import "./NewPost.scss";
import { useDispatch, useSelector } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAdd, faX } from "@fortawesome/free-solid-svg-icons";
// import { toast } from "react-toastify";

// import { toastOption } from "../../variable";
// import { CreatePostAction, UploadFileAction } from "../../redux/PostRedux";

const NewPost = ({ onClose }) => {
    const [images, setImages] = useState([]);
    const [description, setDescription] = useState("");
    const [noComment, setNoComment] = useState(false);

    const dispatch = useDispatch();


    const auth = {};
    const imageRef = useRef();

    const imageChange = (e) => {
        setImages([...images, ...e.target.files]);
    }

    const descriptionChange = (e) => {
        setDescription(e.target.value);
    }

    const removeImage = (i) => {
        const newImages = images.filter((image, index) => (index != i));
        setImages(newImages);
    }


    const handlePost = async () => {
        const newPost = {
            description,
            noComment
        }
        try {
            // const data = new FormData();
            // data.append("image", image);

            // const resUploadImage = await dispatch(UploadFileAction(data));
            // newPost.img = resUploadImage.data;

            // const resCreatePost = await dispatch(CreatePostAction(newPost, auth.token));
            // if (resCreatePost.data) {
            //     toast.info("Đăng bài thành công!", toastOption)
            //     onClose && onClose();
            // }
        } catch (error) {
            console.log(error);
        }
    }

    return (
        <div className="NewPost-Container">
            <div className="title">
                <div className="close-btn" onClick={onClose}>
                    <FontAwesomeIcon icon={faX} />
                </div>
                Tạo bài viết mới
            </div>
            <div className="body">
                <div className="left-container">
                    <div className="left-new-post">
                        <input type="file" multiple ref={imageRef} onChange={imageChange} hidden />
                        {
                            images.length > 0
                                ?
                                <div className="images">
                                    {images.map((image, index) => <div className="image" key={index}>
                                        <img src={URL.createObjectURL(image)} />
                                        <div className="remove-btn" onClick={() => removeImage(index)}>
                                            <FontAwesomeIcon icon={faX} />
                                        </div>
                                    </div>)}

                                    <div className="image add" onClick={() => imageRef.current.click()}>
                                        <FontAwesomeIcon icon={faAdd} />
                                    </div>
                                </div>
                                :
                                <div className="UploadSVG">
                                    <svg aria-label="Biểu tượng thể hiện file phương tiện, chẳng hạn như hình ảnh hoặc video" color="rgb(0, 0, 0)" fill="rgb(0, 0, 0)" height="140" role="img" viewBox="0 0 97.6 77.3" width="180">
                                        <title>Biểu tượng thể hiện file phương tiện, chẳng hạn như hình ảnh hoặc video</title>
                                        <path d="M16.3 24h.3c2.8-.2 4.9-2.6 4.8-5.4-.2-2.8-2.6-4.9-5.4-4.8s-4.9 2.6-4.8 5.4c.1 2.7 2.4 4.8 5.1 4.8zm-2.4-7.2c.5-.6 1.3-1 2.1-1h.2c1.7 0 3.1 1.4 3.1 3.1 0 1.7-1.4 3.1-3.1 3.1-1.7 0-3.1-1.4-3.1-3.1 0-.8.3-1.5.8-2.1z" fill="currentColor"></path>
                                        <path d="M84.7 18.4 58 16.9l-.2-3c-.3-5.7-5.2-10.1-11-9.8L12.9 6c-5.7.3-10.1 5.3-9.8 11L5 51v.8c.7 5.2 5.1 9.1 10.3 9.1h.6l21.7-1.2v.6c-.3 5.7 4 10.7 9.8 11l34 2h.6c5.5 0 10.1-4.3 10.4-9.8l2-34c.4-5.8-4-10.7-9.7-11.1zM7.2 10.8C8.7 9.1 10.8 8.1 13 8l34-1.9c4.6-.3 8.6 3.3 8.9 7.9l.2 2.8-5.3-.3c-5.7-.3-10.7 4-11 9.8l-.6 9.5-9.5 10.7c-.2.3-.6.4-1 .5-.4 0-.7-.1-1-.4l-7.8-7c-1.4-1.3-3.5-1.1-4.8.3L7 49 5.2 17c-.2-2.3.6-4.5 2-6.2zm8.7 48c-4.3.2-8.1-2.8-8.8-7.1l9.4-10.5c.2-.3.6-.4 1-.5.4 0 .7.1 1 .4l7.8 7c.7.6 1.6.9 2.5.9.9 0 1.7-.5 2.3-1.1l7.8-8.8-1.1 18.6-21.9 1.1zm76.5-29.5-2 34c-.3 4.6-4.3 8.2-8.9 7.9l-34-2c-4.6-.3-8.2-4.3-7.9-8.9l2-34c.3-4.4 3.9-7.9 8.4-7.9h.5l34 2c4.7.3 8.2 4.3 7.9 8.9z" fill="currentColor"></path>
                                        <path d="M78.2 41.6 61.3 30.5c-2.1-1.4-4.9-.8-6.2 1.3-.4.7-.7 1.4-.7 2.2l-1.2 20.1c-.1 2.5 1.7 4.6 4.2 4.8h.3c.7 0 1.4-.2 2-.5l18-9c2.2-1.1 3.1-3.8 2-6-.4-.7-.9-1.3-1.5-1.8zm-1.4 6-18 9c-.4.2-.8.3-1.3.3-.4 0-.9-.2-1.2-.4-.7-.5-1.2-1.3-1.1-2.2l1.2-20.1c.1-.9.6-1.7 1.4-2.1.8-.4 1.7-.3 2.5.1L77 43.3c1.2.8 1.5 2.3.7 3.4-.2.4-.5.7-.9.9z" fill="currentColor"></path>
                                    </svg>
                                    <div className="upload-btn" onClick={() => imageRef.current.click()}>
                                        <p>Chọn ảnh từ thiết bị</p>
                                    </div>
                                </div>
                        }
                    </div>
                </div>
                <div className="right-new-post">
                    <div className="owner">
                        <div className="image">
                            <img src="https://pixlr.com/images/index/remove-bg.webp" alt="" />
                        </div>
                        <div className="name">
                            User Account
                        </div>
                    </div>
                    <div className="description">
                        <textarea
                            rows={7}
                            placeholder="Viết mô tả"
                            onChange={descriptionChange}
                            value={description}
                        ></textarea>
                    </div>

                    <div className="options">
                        <div className="options-title">
                            Tùy chọn
                        </div>
                        {/* <div className="option">
                            Ẩn lượt thích
                            <SwitchButton onSwitch={() => setHideLikeNumber(!hideLikeNumber)} />
                        </div> */}
                        <div className="option">
                            Tắt bình luận
                            <SwitchButton onSwitch={() => setNoComment(!noComment)} />
                        </div>
                    </div>
                    <div className="post-btn">
                        <p onClick={handlePost}>
                            Chia sẻ
                        </p>
                    </div>
                </div>
            </div>
        </div >
    );
}

export default NewPost;