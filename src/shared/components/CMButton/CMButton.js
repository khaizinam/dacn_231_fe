import React, { useState, useEffect } from "react";
import "./style.scss";
import { NavLink } from "react-router-dom";

export default function CMButton(props) {
  const [active, setActive] = useState(props.active);
  useEffect(() => {
    setActive(props.active);
  }, [props.active]);

  // const callBack = () => {
  //   if (props?.click) props.click();
  // };

  return (
    <NavLink
      to={props?.link}
      className={active ? "cm-btn-menu-active" : "cm-btn-menu"}
    >
      {props.content}
    </NavLink>
  );
}
