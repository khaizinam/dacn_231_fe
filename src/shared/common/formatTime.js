export const showTime = (time) => {
    let now = new Date();
    let from = new Date(time);
    const newNow = {
        year: now.getFullYear(),
        month: now.getMonth(),
        date: now.getDate(),
    }

    const newFrom = {
        year: from.getFullYear(),
        month: from.getMonth(),
        date: from.getDate(),
    }

    if (newNow.year == newFrom.year) {
        if (newNow.month == newFrom.month && newNow.date == newFrom.date) {

            let diff = now.getTime() - from.getTime();
            const hours = Math.floor(diff / 3600000);
            if (hours > 0) {
                return `${hours} giờ trước`
            } else {
                diff = diff % 3600000;
                const minutes = Math.floor(diff / 60000);
                if (minutes > 0) {
                    return `${minutes} phút trước`
                } else {
                    return "vài giây trước"
                }
            }
        } else {
            return `${newFrom.date} tháng ${newFrom.month + 1}, lúc ${from.getHours()}:${from.getMinutes()}`
        }
    } else {
        return `${newFrom.date} tháng ${newFrom.month + 1}, ${newFrom.year}`
    }
}