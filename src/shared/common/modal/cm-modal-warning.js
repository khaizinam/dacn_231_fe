import React, { useEffect, useRef, useState } from "react";
import { CONST_ITEM } from "../../constants/message";
import "./style.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faExclamation } from "@fortawesome/free-solid-svg-icons";

export default function CMModalWarning(props) {
  const modalRef = useRef();
  const [body, setBody] = useState(props.data.body);

  useEffect(() => {
    if (props.data?.show) {
      modalRef.current.style.display = "block";
    } else {
      modalRef.current.style.display = "none";
    }
    setBody(props.data.body);
  }, [props.data]);

  const handleClose = () => {
    modalRef.current.style.display = "none";
    setBody("");
    if (props?.handleClose)
      props.handleClose({
        ...props.data,
        show: false,
        body: "",
        action: CONST_ITEM.ACTION_CLOSE,
      });
  };
  return (
    <div ref={modalRef} className="modal-container">
      <div className="modal-background" onClick={handleClose}></div>
      <div className="cm-modal" id="cm-modal-confirm">
        <div className="cm-modal-content">
          <div className="cm-modal-header justify-center">
            <div className="cm-modal-title ">
              <FontAwesomeIcon
                className="cm-icon-warning"
                icon={faExclamation}
              />
            </div>
          </div>
          <div className="cm-modal-body">{body}</div>
          <div className="cm-modal-footer">
            <button onClick={handleClose} className="btn btn-error">
              Đóng
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
