import React, { useEffect, useState } from "react";
import "./style.scss";

import CMModalWarning from "./cm-modal-warning";
import CMModalInfo from "./cm-modal-info";
import CMModalConfirm from "./cm-modal-confirm";

const MODAL = {
  WARNING: "warning",
  CONFIRM: "confirm",
  INFO: "info",
};

export default function CMModalComponent({ data, handleClose }) {
  const [typeModal, setTypeModal] = useState(data.type);
  const [childData, setChildData] = useState(data);
  useEffect(() => {
    setTypeModal(data.type || "");
    setChildData(data);
  }, [data]);

  const handleCallBack = (param) => {
    handleClose && handleClose({ ...param });
  };

  switch (typeModal) {
    case MODAL.WARNING:
      return <CMModalWarning handleClose={handleCallBack} data={childData} />;
    case MODAL.CONFIRM:
      return <CMModalConfirm handleClose={handleCallBack} data={childData} />;
    case MODAL.INFO:
      return <CMModalInfo handleClose={handleCallBack} data={childData} />;
    default:
      return <CMModalInfo handleClose={handleCallBack} data={childData} />;
  }
}
