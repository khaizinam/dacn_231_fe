import axios from "axios";
export default class BaseService {
  constructor() {
    this.updateHeader();
  }
  updateHeader() {
    this.header = {
      Authorization: "Bearer " + this.getCookie("jwt"),
      "Access-Control-Allow-Origin": "*",
      "Content-type": "application/json; charset=UTF-8",
      "Access-Control-Allow-Methods": "GET,PUT,POST,DELETE",
      "Access-Control-Allow-Headers": "Content-Type",
      "Access-Control-Allow-Credentials": "true",
    };
  }
  setToken(token) {
    document.cookie = "";
    this.setCookie("jwt", token);
  }
  getCookie(cname) {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(";");
    for (let i = 0; i < ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0) === " ") {
        c = c.substring(1);
      }
      if (c.indexOf(name) === 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }
  clearCookie() {
    this.setCookie("jwt", "");
  }
  setCookie(cname, cvalue, exdays = 1) {
    const d = new Date();
    d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
    let expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }
  post(url, payload) {
    this.updateHeader();
    return axios.post(url, payload, { headers: this.header });
  }
  get(url, header) {
    this.updateHeader();
    return axios.get(url, { headers: { ...header, ...this.header } });
  }
  put(url, payload) {
    this.updateHeader();
    return axios.put(url, payload, { headers: this.header });
  }
  delete(url, payload) {
    this.updateHeader();
    return axios.delete(url, payload, { headers: this.header });
  }
}
