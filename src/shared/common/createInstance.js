import axios from "axios";
import jwt_decode from "jwt-decode";
//import { loginSuccess } from "./redux/authSlice";

export const createAxios = (user) => {
  let newInstance = axios.create();
  newInstance.interceptors.request.use(
    async (config) => {
      let currDate = new Date();
      const decodeToken = jwt_decode(user?.token);
      if (decodeToken.exp < currDate.getTime() / 1000) {
      } else {
        config.headers["authorization"] = "Bearer " + user?.token;
      }
      return config;
    },
    (error) => {
      return Promise.reject(error);
    }
  );
  return newInstance;
};
