import actionTypes from "../actions/actionTypes"

const initialState = {
    login: {
        isLoggedIn: false,
        currentUser: null,
        isFetching: false,
        error: false,
        errMessage: ""
    },
    logout: {
        isFetching: false,
        error: false
    }
}

const authReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.USER_LOGIN_START:
            return {
                ...state,
                login: {
                    isLoggedIn: false,
                    currentUser: null,
                    isFetching: true,
                    error: false,
                    errMessage: ""
                },
            }
        case actionTypes.USER_LOGIN_SUCCESS:
            return {
                ...state,
                login: {
                    isLoggedIn: true,
                    currentUser: action.userInfo,
                    isFetching: false,
                    error: false,
                    errMessage: ""
                }
            }
        case actionTypes.USER_LOGIN_FAIL:
            return {
                ...state,
                login: {
                    isLoggedIn: false,
                    currentUser: null,
                    isFetching: false,
                    error: true,
                    errMessage: action.errMessage
                }
            }
        case actionTypes.USER_LOGOUT_START:
            return {
                ...state,
                logout: {
                    isFetching: true,
                    error: false
                }
            }
        case actionTypes.USER_LOGOUT_SUCCESS:
            return {
                ...state,
                login: {
                    ...state.login,
                    isLoggedIn: false,
                    currentUser: null,
                },
                logout: {
                    isFetching: false,
                    error: false
                }
            }
        case actionTypes.USER_LOGOUT_FAIL:
            return {
                ...state,
                logout: {
                    isFetching: false,
                    error: true
                }
            }
        default:
            return state;
    }
}

export default authReducer;