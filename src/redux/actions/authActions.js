import AuthService from "../../shared/services/authService";
import actionTypes from "./actionTypes";
// import { createAxios } from "../../shared/common/createInstance";
import { SCREEN } from "../../shared/constants/router.constant";

const { URL_CONST } = require("../../shared/environment");
const authService = new AuthService();
export const userLogin = (email, password, navigate) => {
  return async (dispatch) => {
    try {
      dispatch(userLoginStart());
      let res = await authService.login(email, password);
      if (res && res.data && res.data.data) {
        dispatch(userLoginSuccess(res.data.data));
        authService.setToken(res.data.data.token);
        navigate(SCREEN.homepage.path);
        return;
      } else {
        dispatch(userLoginFail("Lỗi hệ thống"));
        return "Lỗi hệ thống";
      }
    } catch (e) {
      dispatch(userLoginFail(e.response?.data?.data?.message));
      console.log("Login error", e);
      return e.response?.data?.data?.message;
    }
  };
};

export const userLoginStart = () => ({
  type: actionTypes.USER_LOGIN_START,
});

export const userLoginSuccess = (userInfo) => ({
  type: actionTypes.USER_LOGIN_SUCCESS,
  userInfo: userInfo,
});

export const userLoginFail = (errMessage) => ({
  type: actionTypes.USER_LOGIN_FAIL,
  errMessage: errMessage,
});

export const userLogout = (token, userInfo, navigate) => {
  return async (dispatch) => {
    try {
      dispatch(userLogoutStart());
      //const axiosJWT = createAxios(userInfo);
      await authService.logout(
        URL_CONST.SERVER_URL + URL_CONST.AUTH.LOGOUT,
        {}
      );
      dispatch(userLogoutSuccess());
      await authService.clearCookie();
      navigate(SCREEN.loginPage.path);
    } catch (e) {
      dispatch(userLogoutFail());
      console.log("Logout error", e);
    }
  };
};

export const userLogoutStart = () => ({
  type: actionTypes.USER_LOGOUT_START,
});

export const userLogoutSuccess = (userInfo) => ({
  type: actionTypes.USER_LOGOUT_SUCCESS,
  userInfo: userInfo,
});

export const userLogoutFail = () => ({
  type: actionTypes.USER_LOGOUT_FAIL,
});
