import React, { useEffect, useState } from "react";
import "./loginPage.scss";
import { SCREEN } from "../../shared/constants/router.constant";
import { useDispatch } from "react-redux";
import { userLogin } from "../../redux/actions/authActions";
import { useNavigate } from "react-router-dom";
import { formatString } from "../../shared/common/formatString";
import { CONST_MESSAGE } from "../../shared/constants/message";

export default function LoginPage() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const errorMsg = {
    email: {
      isError: false,
      msg: useState(""),
    },
    password: {
      isError: false,
      msg: useState(""),
    },
    server: {
      isError: false,
      msg: useState(""),
    },
  };

  // const stateLogin = useSelector((state) => state?.auth?.login);
  //const auth = useSelector(state => state.auth);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  useEffect(() => {
    document.title = SCREEN.loginPage.title;
  }, []);

  const validationSaveItem = async () => {
    let valid = true;
    if (!email) {
      // has email
      errorMsg.email.isError = true;
      errorMsg.email.msg[1](formatString(CONST_MESSAGE.REQUIRED.vn, ["Email"]));
      valid = false;
    }

    // validation type email @example.com
    return validationSavePassword(valid);
  };

  const validationSavePassword = async (valid) => {
    if (!password) {
      errorMsg.email.isError = true;
      errorMsg.password.msg[1](
        formatString(CONST_MESSAGE.REQUIRED.vn, ["Mật khẩu"])
      );
      valid = false;
    }
    return valid;
  };

  const handleLogin = async () => {
    errorMsg.email.isError = false;
    errorMsg.password.isError = false;
    errorMsg.server.isError = false;
    errorMsg.password.msg[1]("");
    errorMsg.email.msg[1]("");
    errorMsg.server.msg[1]("");

    const valid = await validationSaveItem();
    if (!valid) return;
    else {
      let result = await dispatch(userLogin(email, password, navigate));
      if (result) {
        errorMsg.server.isError = true;
        errorMsg.server.msg[1](result);
      }
    }
  };

  const handleKeyDown = (event) => {
    if (event.key === "Enter" || event.keyCode === 13) {
      handleLogin();
    }
  };

  const redirectSignup = () => {
    navigate(SCREEN.registerPage.path);
  };

  return (
    <div className="login-background">
      <div className="login-container">
        <form autoComplete="off" className="login-content row">
          <div className="col-12 text-center text-login">Đăng nhập</div>
          <div className="col-12 form-group login-input">
            <label>Email</label>
            <input
              type="text"
              name="email"
              className="form-control"
              placeholder="Vui lòng nhập email!"
              onChange={(event) => setEmail(event.target.value)}
              onKeyDown={(event) => handleKeyDown(event)}
            />
          </div>
          <div className="col-12" style={{ color: "red" }}>
            {errorMsg.email.msg[0]}
          </div>
          <div className="col-12 form-group login-input">
            <label>Mật khẩu</label>
            <div className="custom-input-password">
              <input
                className="form-control"
                type="password"
                name="password"
                placeholder="Vui lòng điền mật khẩu"
                onChange={(event) => setPassword(event.target.value)}
                onKeyDown={(event) => handleKeyDown(event)}
              />
            </div>
          </div>
          <div className="col-12" style={{ color: "red" }}>
            {errorMsg.password.msg[0]}
          </div>
          <div className="col-12" style={{ color: "red" }}>
            {errorMsg.server.msg[0]}
          </div>
          <div className="col-12">
            <button
              type="submit"
              className="btn-login"
              onClick={() => handleLogin()}
            >
              Đăng nhập
            </button>
          </div>
          <div className="col-12 activity">
            <span className="forgot-password">Quên mật khẩu?</span>
            <span onClick={() => redirectSignup()} className="forgot-password">
              Bạn chưa có tài khoản?
            </span>
          </div>
        </form>
      </div>
    </div>
  );
}
