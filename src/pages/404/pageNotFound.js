import React, { useEffect } from "react";
import { SCREEN } from "../../shared/constants/router.constant";
import "./style.scss";
export default function PageNotFound() {
  useEffect(() => {
    document.title = SCREEN._404.title;
  }, []);

  return (
    <div>
      <h1>PAGE NOT FOUND</h1>
      <div>
        <a href="/">Về lại trang chủ</a>
      </div>
    </div>
  );
}
