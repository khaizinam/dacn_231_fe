import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { SCREEN } from "../shared/constants/router.constant";
import AuthService from "../shared/services/authService";

const authService = new AuthService();
export default function S02Component() {
  const navigate = useNavigate();
  const [message, setMessage] = useState("");
  useEffect(() => {
    document.title = SCREEN.s02.title;
    checkPermission();
  }, [navigate]);
  const checkPermission = async () => {
    try {
      const response = await authService.initPermission();
      const content = response?.data.data;
      setMessage(
        `Hi ${content.full_name}, bạn đã đăng nhập với email : ${content.email}`
      );
    } catch (error) {
      if (error?.response) {
        setMessage(`Bạn chưa đăng nhập`);
      } else {
        setMessage(`Lỗi server`);
      }
    }
  };
  const handleClick = () => {
    authService.logOut();
    navigate(SCREEN.dev.path);
  };
  return (
    <div className="App">
      <h1>S02 page</h1>
      <p>{message}</p>
      <a href="/dev">Về lại trang test</a>
      <button onClick={handleClick}>Log out</button>
    </div>
  );
}
