import React, { useEffect, useState } from "react";
import { SCREEN } from "../../shared/constants/router.constant";
import "./style.scss";
import CMButton from "../../shared/components/CMButton/CMButton";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars } from "@fortawesome/free-solid-svg-icons";
import { Route, Routes, useLocation, useNavigate } from "react-router-dom";
import Tree from "./components/tree/tree";
import Reminder from "./components/reminder";
import WaterPot from "./components/water-area/waterPot";
import IOT from "./components/iot";

const pathLST = [
  {
    path: `tree`,
    msg: "Cây của tôi",
    mode: 1,
    component: <Tree />,
  },
  {
    path: `reminder`,
    msg: "Nhắc nhở",
    mode: 2,
    component: <Reminder />,
  },
  {
    path: `water-pot`,
    msg: "Tưới nước",
    mode: 3,
    component: <WaterPot />,
  },
  {
    path: `iot`,
    msg: "Thiết bị",
    mode: 4,
    component: <IOT />,
  },
];
export default function Manager() {
  const navigate = useNavigate();
  const location = useLocation();
  console.log(location.pathname);
  const findPath = () => {
    let mode = pathLST.find(
      (val) => location.pathname === `/manage/${val.path}`
    );
    if (!mode) {
      mode = pathLST[0];
    }
    return mode;
  };
  const pathObj = findPath();
  const [active, setActive] = useState(pathObj.mode);

  useEffect(() => {
    document.title = SCREEN.manager.title;

    /*  check right url  */
    let mode = pathLST.find(
      (val) => location.pathname === `/manage/${val.path}`
    );
    if (!mode) {
      mode = pathLST[0];
      navigate(mode.path);
    }
    setActive(mode.mode);
    /* end of check right url */
  }, [location.pathname, navigate]);

  /* list button */
  const buttons = [];
  const routers = [];
  pathLST.forEach((key, index) => {
    buttons.push(
      <CMButton
        key={index}
        link={key.path}
        content={key.msg}
        active={active === key.mode}
      />
    );
    routers.push(<Route key={index} path={key.path} element={key.component} />);
  });

  return (
    <div className="bg">
      <div className="wr-bar-top">
        <div className="wr-bar-top-1 container"></div>
      </div>
      <div className="wr-bar-1">
        <div className="container button-holder">
          <div>
            <div className="side-left">{buttons}</div>
          </div>
          <div>
            <div className="side-right">
              <CMButton
                content={
                  <FontAwesomeIcon icon={faBars} style={{ fontSize: 35 }} />
                }
                active={false}
              />
            </div>
          </div>
        </div>
      </div>
      <div className="container wrapper-content">
        <Routes>{routers}</Routes>
      </div>
    </div>
  );
}
