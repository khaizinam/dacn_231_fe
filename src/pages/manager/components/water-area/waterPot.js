import React, { useEffect, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faDroplet,
  faBell,
  faFaucetDrip,
  faTriangleExclamation,
  faList,
  faTableCellsLarge,
} from "@fortawesome/free-solid-svg-icons";
const treeAPI = [
  {
    id: 1,
    name: "Bon Sai",
    content: "lorem ispum ",
    alerts: [0, 1, 2, 3],
  },
  {
    id: 2,
    name: "Rau sống ngoài vườn",
    content:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem",
    alerts: [2, 3],
  },
  {
    id: 3,
    name: "Rau sống 1",
    content:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem",
    alerts: [0, 3],
  },
  {
    id: 41,
    name: "Rau sống",
    content: "Trồng ngoài ban công.",
    alerts: [0, 3],
  },
  {
    id: 61,
    name: "Rau sống",
    content: "Trồng ngoài sân.",
    alerts: [1, 3],
  },
];
const userSetting = {
  viewMode: 1,
};
export default function WaterPot() {
  function getAlert(alerts, id) {
    const alertLst = [];
    alerts.forEach((ele, index) => {
      switch (ele) {
        case 0:
          alertLst.push(
            <span key={`alert-${id}-${index}`} className="icon icon-droplet">
              <FontAwesomeIcon icon={faDroplet} />
            </span>
          );
          break;
        case 1:
          alertLst.push(
            <span key={`alert-${id}-${index}`} className="icon icon-alarm">
              <FontAwesomeIcon icon={faBell} />
            </span>
          );
          break;
        case 2:
          alertLst.push(
            <span
              key={`alert-${id}-${index}`}
              className="icon icon-faucet-drip"
            >
              <FontAwesomeIcon icon={faFaucetDrip} />
            </span>
          );
          break;
        case 3:
          alertLst.push(
            <span key={`alert-${id}-${index}`} className="icon icon-error">
              <FontAwesomeIcon icon={faTriangleExclamation} />
            </span>
          );
          break;
        default:
          break;
      }
    });

    return alertLst;
  }
  function ItemModeBox(data) {
    const items = [];
    data.forEach((element, index) => {
      items.push(
        <div
          key={`area-${element.id}`}
          id={`area-${element.id}`}
          className="box"
          onClick={() => {
            console.log(`click on id ${element.id}`);
          }}
        >
          <div className="box-body">
            <div className="tree-head">
              <span className="tree-name">{element.name}</span>
              <span className="tree-content">{element.content}</span>
            </div>
            <div className="tree-alarm">
              {getAlert(element.alerts, element.id)}
            </div>
          </div>
        </div>
      );
    });
    return items;
  }
  function ItemModeList(data) {
    const items = [];
    data.forEach((element, index) => {
      items.push(
        <div
          key={`area-${element.id}`}
          id={`area-${element.id}`}
          className={index % 2 === 0 ? "row" : "row bg-gray"}
          onClick={() => {
            console.log(`click on id ${element.id}`);
          }}
        >
          <span className="row-name">{element.name}</span>
          <span
            className={
              index % 2 === 0
                ? "row-content"
                : "row-content border-left-right-white"
            }
          >
            {element.content}
          </span>
          <div className="row-alarms">
            {getAlert(element.alerts, element.id)}
          </div>
        </div>
      );
    });
    return items;
  }
  const [viewMode, setViewMode] = useState(userSetting.viewMode);
  const [viewList, setViewList] = useState(ItemModeBox(treeAPI));
  useEffect(() => {
    if (viewMode === 1) setViewList(ItemModeBox(treeAPI));
    else setViewList(ItemModeList(treeAPI));
  }, [viewMode]);
  return (
    <div className="container">
      <div className="bar-mode-view">
        <button
          onClick={() => setViewMode(1)}
          className={viewMode === 0 ? "icon-no-active" : "icon"}
        >
          <FontAwesomeIcon icon={faTableCellsLarge} />
        </button>
        <button
          onClick={() => setViewMode(0)}
          className={viewMode === 1 ? "icon-no-active" : "icon"}
        >
          <FontAwesomeIcon icon={faList} />
        </button>
      </div>
      <div className="wr-view">{viewList}</div>
    </div>
  );
}
