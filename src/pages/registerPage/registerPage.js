import React, { useEffect, useState } from "react";
import "./registerPage.scss";
import { SCREEN } from "../../shared/constants/router.constant";
import AuthService from "../../shared/services/authService";
import { useNavigate } from "react-router-dom";
import {
  CONST_ITEM,
  CONST_MESSAGE,
  CONST_VALIDATE,
} from "../../shared/constants/message";
import CMModalComponent from "../../shared/common/modal/cm-modal";
import { formatString } from "../../shared/common/formatString";
const authService = new AuthService();
export default function RegisterPage() {
  const [email, setEmail] = useState("");
  const [fullName, setFullName] = useState("");
  const [password, setPassword] = useState("");
  const [modal, setModal] = useState({ show: false, body: "" }); // set up modal
  const CMModal = async (param) => {
    if (param?.callBack) {
      await param.callBack(param.action);
    }
  };
  const errorMsg = {
    email: {
      msg: useState(""),
    },
    fullName: {
      msg: useState(""),
    },
    password: {
      msg: useState(""),
    },
    server: {
      msg: useState(""),
    },
  };
  const navigate = useNavigate();

  useEffect(() => {
    document.title = SCREEN.registerPage.title;
  }, []);
  /* ---VALIDATION--- */
  const validationSaveItem = () => {
    errorMsg.email.msg[1]("");
    errorMsg.fullName.msg[1]("");
    errorMsg.password.msg[1]("");
    errorMsg.server.msg[1]("");
    let valid = true;
    if (!email) {
      errorMsg.email.msg[1](formatString(CONST_MESSAGE.REQUIRED.vn, ["Email"]));
      valid = false;
    } else if (!CONST_VALIDATE.EMAIL.test(email)) {
      errorMsg.email.msg[1](formatString(CONST_MESSAGE.IN_VALID.vn, ["Email"]));
      valid = false;
    }
    return validationSaveFullName(valid);
  };
  function validationSaveFullName(valid) {
    if (!fullName) {
      errorMsg.fullName.msg[1](
        formatString(CONST_MESSAGE.REQUIRED.vn, ["Họ và tên"])
      );
      valid = false;
    } else if (fullName.length < 6) {
      errorMsg.fullName.msg[1](
        formatString(CONST_MESSAGE.C001.vn, ["Tên người dùng", 6])
      );
      valid = false;
    }
    return validationSavePassword(valid);
  }
  function validationSavePassword(valid) {
    if (!password) {
      errorMsg.password.msg[1](
        formatString(CONST_MESSAGE.REQUIRED.vn, ["Mật khẩu"])
      );
      valid = false;
    } else if (password.length < 6) {
      errorMsg.password.msg[1](
        formatString(CONST_MESSAGE.C001.vn, ["Mật khẩu", 6])
      );
      valid = false;
    }
    return valid;
  }
  /*-------*/
  const handleRegister = async () => {
    //event.preventDefault();
    const valid = validationSaveItem();
    console.log(valid);
    if (!valid) return;
    else {
      try {
        await authService.register(email, password, fullName);
        setModal({
          show: true,
          body: (
            <div
              style={{
                display: "flex",
                justifyContent: "center",
                flexDirection: "column",
              }}
            >
              <p>Chúc mừng {email} đã đăng ký thành công</p>
              <p>Bạn sẽ được chuyển vê trang đăng nhập</p>
            </div>
          ),
          type: CONST_ITEM.MODAL.INFO,
          callBack: (action) => {
            if (action === CONST_ITEM.ACTION_CLOSE) {
              navigate(SCREEN.loginPage.path);
            }
          },
        });
      } catch (error) {
        // gửi đi được
        if (error?.response) {
          setModal({
            show: true,
            body: error.response.data.data.message,
            type: CONST_ITEM.MODAL.WARNING,
          });
        } else {
          setModal({
            show: true,
            body: "Không thể gửi tới server!",
            type: CONST_ITEM.MODAL.WARNING,
          });
        }
      }
    }
  };

  const handleKeyDown = (event) => {
    if (event.key === "Enter" || event.keyCode === 13) {
      handleRegister();
    }
  };

  const redirectLogin = () => {
    navigate(SCREEN.loginPage.path);
  };

  return (
    <div className="register-background">
      <div className="register-container">
        <div className="register-content row">
          <div className="col-12 text-center text-register">
            Đăng kí tài khoản
          </div>
          <div className="col-12 form-group register-input">
            <label>Email</label>
            <input
              type="email"
              name="email"
              autoComplete="new-password"
              className="form-control"
              placeholder="Vui lòng điền email!"
              value={email}
              onChange={(event) => setEmail(event.target.value)}
              onKeyDown={(event) => handleKeyDown(event)}
            />
            <div className="col-12" style={{ color: "red" }}>
              {errorMsg.email.msg[0]}
            </div>
          </div>
          <div className="col-12 form-group register-input">
            <label>Tên người dùng</label>
            <input
              type="text"
              name="full-name"
              autoComplete="new-password"
              className="form-control"
              placeholder="Vui lòng điền tên của bạn!"
              value={fullName}
              onChange={(event) => setFullName(event.target.value)}
              onKeyDown={(event) => handleKeyDown(event)}
            />
            <div className="col-12" style={{ color: "red" }}>
              {errorMsg.fullName.msg[0]}
            </div>
          </div>
          <div className="col-12 form-group register-input">
            <label>Mật khẩu</label>
            <div className="custom-input-password">
              <input
                className="form-control"
                value={password}
                name="password"
                type="password"
                autoComplete="new-password"
                placeholder="Vui lòng điền mật khẩu!"
                onChange={(event) => setPassword(event.target.value)}
                onKeyDown={(event) => handleKeyDown(event)}
              />
            </div>
            <div className="col-12" style={{ color: "red" }}>
              {errorMsg.password.msg[0]}
            </div>
          </div>
          <div className="col-12" style={{ color: "red" }}>
            {errorMsg.server.msg[0]}
          </div>
          <div className="col-12">
            <button className="btn-register" onClick={() => handleRegister()}>
              Đăng kí
            </button>
          </div>
          <div className="col-12 activity">
            <span onClick={() => redirectLogin()} className="forgot-password">
              Bạn đã có tài khoản, trở về trang đăng nhập!
            </span>
          </div>
        </div>
      </div>
      <CMModalComponent handleClose={CMModal} data={modal} />
    </div>
  );
}
