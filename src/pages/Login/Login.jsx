import { Link } from "react-router-dom";
import Logo from "../../shared/components/Logo/Logo";

import { useEffect, useState } from "react";

// import { toast } from "react-toastify";

// import { useDispatch } from "react-redux";
// import { LoginAction } from "../../redux/AuthRedux";

// import { toastOption } from "../../variable";

import "./Login.scss";
import { SCREEN } from "../../shared/constants/router.constant";

const Login = () => {
    // const dispatch = useDispatch();
    const [input, setInput] = useState({
        email: "",
        password: "",
    });

    useEffect(() => {
        document.title = SCREEN.loginPage.title;
    }, []);

    const handleChange = e => {
        setInput({
            ...input, [e.target.name]: e.target.value
        })
    }

    const showError = (msg) => {
        // toast.error(msg, {
        //     position: "bottom-right",
        //     autoClose: 3000,
        //     closeButton: false,
        //     closeOnClick: true,
        // })
    }

    const handleSubmit = async (e) => {
        e.preventDefault();

        try {
            const { email, password } = input;
            // await dispatch(LoginAction({ username, password }));
            // toast.info("Đăng nhập thành công", toastOption)
        } catch (error) {
            console.log(error);
            showError(error.response.data)
        }

    }
    return (
        <div className="Login-Container">

            <form className="Login-Box" onSubmit={handleSubmit}>
                <div className="login-logo">
                    <Logo />
                </div>
                <div className="input-line">
                    <input
                        type="email"
                        placeholder="Email"
                        required
                        name="email"
                        value={input.email}
                        onChange={handleChange}
                    />
                </div>
                <div className="input-line">
                    <input
                        type="password"
                        placeholder="Mật khẩu"
                        required
                        name="password"
                        value={input.password}
                        onChange={handleChange}
                    />
                </div>
                <div className="input-line">
                    <input type="submit" className="login-btn" value="Đăng nhập" />
                </div>

                <div className="input-line or-name">
                    <span></span>
                    <div>HOẶC</div>
                    <span></span>
                </div>

                <div className="input-line register-direct">
                    <p>Bạn chưa có tài khoản ư?</p>
                    <Link to="register">
                        Đăng ký
                    </Link>
                </div>
            </form>
        </div>
    );
}

export default Login;