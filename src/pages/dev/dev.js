import React, { useEffect, useState } from "react";
import UserService from "../../shared/services/userService";
import { SCREEN } from "../../shared/constants/router.constant";
import "./dev.scss";
import { formatString } from "../../shared/common/formatString";
import { CONST_ITEM, CONST_MESSAGE } from "../../shared/constants/message";
import AuthService from "../../shared/services/authService";
import CMModalComponent from "../../shared/common/modal/cm-modal";
// import { useNavigate } from "react-router-dom";
const userService = new UserService();
const authService = new AuthService();
const lan = "vn";
export default function DevComponent(props) {
  useEffect(() => {
    document.title = SCREEN.dev.title;
    checkPermission();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  const [response, setResponse] = useState("");
  const [loginInfo, setLoginInfo] = useState("");
  // call modal
  const [modal, setModal] = useState({ show: false, body: "" });
  // check login
  const checkPermission = async () => {
    try {
      const response = await authService.initPermission();
      const content = response?.data.data;
      setLoginInfo(
        `Hi ${content.full_name}, bạn đã đăng nhập với email : ${content.email}`
      );
      setModal({
        show: true,
        body: `Hi ${content.full_name}, bạn đã đăng nhập với email : ${content.email}`,
        type: CONST_ITEM.MODAL.INFO,
      });
    } catch (error) {
      setLoginInfo(`Bạn chưa đăng nhập/ Token bị sai!`);
      setModal({
        show: true,
        body: "Bạn chưa đăng nhập/ Token bị sai!",
        type: CONST_ITEM.MODAL.WARNING,
      });
    }
  };
  const errorMessage = {
    email: useState(""),
    password: useState(""),
  };

  const validationSaveItem = () => {
    errorMessage.email[1]("");
    errorMessage.password[1]("");
    let valid = true;
    const email = document.getElementById("email").value;
    if (!email) {
      errorMessage.email[1](
        formatString(CONST_MESSAGE.REQUIRED[lan], ["Email"])
      );
      valid = false;
    }
    return validationSavePassword(valid);
  };

  const validationSavePassword = (valid) => {
    const password = document.getElementById("password").value;
    if (!password) {
      errorMessage.password[1](
        formatString(CONST_MESSAGE.REQUIRED[lan], ["Password"])
      );
      valid = false;
    }
    return valid;
  };

  const handleClick = async () => {
    const valid = validationSaveItem();
    console.log(valid);
    if (!valid) return;
    try {
      const email = document.getElementById("email").value;
      const password = document.getElementById("password").value;
      const response = await userService.onLogin({
        email,
        password,
      });
      const content = response?.data.data;
      setResponse(JSON.stringify(content));
      setLoginInfo(
        `Hi ${content.full_name}, bạn đã đăng nhập với email : ${content.email}`
      );
      setModal({
        show: true,
        body: `Hi ${content.full_name}, bạn đã đăng nhập với email : ${content.email}`,
        type: CONST_ITEM.MODAL.INFO,
      });
      userService.setToken(content.token);
    } catch (error) {
      if (error?.response) {
        const content = error.response.data;
        setResponse(JSON.stringify(content));
      } else {
        setResponse("lỗi server");
      }
      setModal({
        show: true,
        body: "lỗi server",
        type: CONST_ITEM.MODAL.WARNING,
      });
    }
  };
  const CMModal = async (param) => {
    if (param?.callBack) {
      console.log(param);
      await param.callBack(param.action);
    }
  };
  const handleClearToken = async () => {
    setModal({
      show: true,
      body: "Bạn có muốn xoá token!",
      type: CONST_ITEM.MODAL.CONFIRM,
      callBack: (action) => {
        if (action === CONST_ITEM.ACTION_OK) {
          authService.logOut();
          setLoginInfo(`Bạn chưa đăng nhập/ Token bị sai!`);
        }
      },
    });
  };
  const listRouter = [];
  Object.keys(SCREEN).forEach((key, index) => {
    listRouter.push(
      <div key={`page-wr-${index}`}>
        <a key={`page-${index}`} href={SCREEN[key].path}>
          {SCREEN[key].title}
        </a>
      </div>
    );
  });

  return (
    <div>
      <div className="pages-lst"> {listRouter}</div>
      <div className="pages-lst">{loginInfo}</div>
      <div className="cm-form-controls">
        <input id="email" placeholder="Please input your email" />
        <div className="cm-invalid">{errorMessage.email[0]}</div>
        <input id="password" placeholder="Please input your password" />
        <div className="cm-invalid">{errorMessage.password[0]}</div>
        <button onClick={handleClick}>Login</button>
        <button onClick={handleClearToken}>Clear Token</button>
      </div>
      <div className="box-err-msg">
        <p>Response data</p>
        <p>{response}</p>
      </div>
      <CMModalComponent handleClose={CMModal} data={modal} />
    </div>
  );
}
