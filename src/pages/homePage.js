import React, { useEffect } from "react";
import { SCREEN } from "../shared/constants/router.constant";
import { useDispatch, useSelector } from "react-redux";
import { userLogout } from "../redux/actions/authActions";
import { useNavigate } from "react-router-dom";
function HomePage() {
  useEffect(() => {
    document.title = SCREEN.homepage.title;
  }, []);

  const currentUser = useSelector((state) => state?.auth?.login?.currentUser);
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const handleLogout = () => {
    dispatch(userLogout(currentUser.token, currentUser, navigate));
  };

  return (
    <>
      {!currentUser ? (
        <div className="App">
          <h1>Home Page</h1>
          <p>Không check API</p>
          <p>
            <a href="/dev">TEST LOGIN PAGE</a>
          </p>
          <p>
            <a href="/login">LOGIN PAGE</a>
          </p>
          <p>
            <a href={SCREEN.manager.path}>Tree manager</a>
          </p>
        </div>
      ) : (
        <div className="App">
          <span style={{ color: "blue" }}>Hello {currentUser.email}</span>
          <br />
          <button onClick={() => handleLogout()}>Đăng xuất</button>
        </div>
      )}
    </>
  );
}

export default HomePage;
