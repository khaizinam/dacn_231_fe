import React, { useEffect, useState } from "react";
import { SCREEN } from "../shared/constants/router.constant";
import AuthService from "../shared/services/authService";
import { useNavigate } from "react-router-dom";

const authService = new AuthService();

export default function MainPage() {
  const [message, setMessage] = useState("");
  const navigate = useNavigate();
  useEffect(() => {
    document.title = SCREEN.loginPage.title;
    checkPermission();
  }, [navigate]);
  const checkPermission = async () => {
    try {
      const response = await authService.initPermission();
      const content = response?.data.data;
      setMessage(
        `Hi ${content.full_name}, bạn đã đăng nhập với email : ${content.email}`
      );
    } catch (error) {
      if (error?.response) {
        setMessage(`Bạn chưa đăng nhập`);
      } else {
        setMessage(`Lỗi server`);
      }
    }
  };
  return (
    <div>
      <div>Main page</div>
      <a href="/dev">Về lại trang test</a>
      <p>{message}</p>
    </div>
  );
}
